package com.epam.courses.util;

import java.util.regex.Pattern;

public class Const {
  public static final Pattern SENTENCE_PATTERN = Pattern.compile("[^.!?]*[.!?]");
  public static final Pattern WORD_PATTERN = Pattern.compile("\\b[a-zA-Z']+\\b");
  public static final Pattern SYMBOL_PATTERN = Pattern.compile("[^\\w]'");
  public static final Pattern QUESTION_PATTERN = Pattern.compile(".*\\?");
  public static final Pattern VOWEL_PATTERN = Pattern.compile("\\b[aieouAIEOU]\\w*");
}
