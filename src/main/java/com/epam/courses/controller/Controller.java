package com.epam.courses.controller;

import com.epam.courses.model.Book;
import com.epam.courses.model.BookManager;
import com.epam.courses.model.Sentence;
import com.epam.courses.model.SentenceManager;
import com.epam.courses.model.Word;
import com.epam.courses.util.Const;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

public class Controller {
    private Book book;
    private BookManager bookManager;

  public Controller() {
    this.book = new Book("src/main/resources/1.txt");
    this.bookManager = new BookManager(book);
  }


  public long firstTask() {
    return bookManager.numberOfSentencesWithRepitedWords();
  }

  public List<String> secondTask() {
    return bookManager.sortSentencesByWordsCount().stream()
        .map(sentence -> sentence.getSentenceText())
        .collect(Collectors.toList());
  }

  public List<String> thirdTask() {
    List<Sentence> sentences = bookManager.getBook().getSentences();
    List<Word> wordsInFirstSentence = sentences.get(0).getWordList();
    List<String> resultedWordList = new ArrayList<>();
    for (Word word : wordsInFirstSentence) {
      boolean presentWordInSentences = false;
      for (int i = 1; i < sentences.size(); i++) {
        SentenceManager sentenceManager = new SentenceManager(sentences.get(i));
        if (sentenceManager.hasWordOccurences(word)) {
          presentWordInSentences = true;
          break;
        }
      }
      if (!presentWordInSentences) {
        resultedWordList.add(word.getWordText());
      }
    }
    return resultedWordList;
  }

  public List<String> fourthTask(Integer length) {
    return bookManager.getBook().getSentences().stream()
        .map(SentenceManager::new)
        .filter(SentenceManager::isQuestionSentence)
        .flatMap(sentenceManager -> sentenceManager.findAllWithLength(length).stream())
        .map(Word::getWordText)
        .collect(Collectors.toList());
  }

  public List<String> fifthTask() {
    List<Sentence> sentences = bookManager.getBook().getSentences();
    List<String> sentencesWithReplasedWords = new ArrayList<>();
    for (Sentence sentence : sentences) {
      SentenceManager sentenceManager = new SentenceManager(sentence);
      Iterator<Word> iterator = sentenceManager
          .findAllWithLength(sentenceManager.maximumWordLength())
          .iterator();
      Word wordWithMaximumLength = iterator.hasNext() ? iterator.next() : null;
      iterator = sentence.getWordsByRegexPattern(Const.VOWEL_PATTERN).iterator();
      Word firstWordWhatBeginWithVowel = iterator.hasNext() ? iterator.next() : null;
      String resultedSentence = sentenceManager.replaceTwoWords(wordWithMaximumLength, firstWordWhatBeginWithVowel);
      if (!resultedSentence.equals(sentence.getSentenceText())){
        sentencesWithReplasedWords.add(resultedSentence);
      }
    }
    return sentencesWithReplasedWords;
  }


  public void fodo() {
  }
}
