package com.epam.courses.model;

import static java.util.stream.Collectors.toMap;

import com.epam.courses.util.Const;
import java.util.AbstractMap.SimpleEntry;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

public class SentenceManager {
  private Sentence sentence;

  public SentenceManager(Sentence sentence) {
    this.sentence = sentence;
  }

  public Map<Word, Integer> countOfWords() {
    return sentence.getWordList().stream()
        .map(word -> new SimpleEntry<>(word, 1))
        .collect(toMap(SimpleEntry::getKey, SimpleEntry::getValue, Integer::sum));
  }

  public long countOfWordsRepetition() {
    return countOfWords().values().stream()
        .filter(count -> count > 1)
        .count();
  }

  public long wordOccurences(Word word) {
    return sentence.getWordList().stream()
        .filter(wordFromStream -> wordFromStream.equals(word))
        .count();
  }

  public boolean hasWordOccurences(Word word) {
    return wordOccurences(word) > 0;
  }

  public boolean isQuestionSentence() {
    return Const.QUESTION_PATTERN.matcher(sentence.getSentenceText()).matches();
  }

  public List<Word> findAllWithLength(int length) {
    return sentence.getWordList().stream()
        .filter(word -> word.getWordText().length() == length)
        .collect(Collectors.toList());
  }

  public int maximumWordLength() {
    return sentence.getWordList().stream()
        .map(word -> word.getWordText().length())
        .mapToInt(length -> length)
        .max()
        .orElse(0);
  }
  public String replaceTwoWords(Word word1, Word word2) {
    if ((word1 != null) && (word2 != null) && (sentence.getWordList().contains(word1)) && (sentence
        .getWordList().contains(word1)) && !word1.equals(word2)) {
      if (word1.getPositionOfLastLetter() > word2.getPositionOfLastLetter()) {
        Word tempWord = word1;
        word1 = word2;
        word2 = tempWord;
      }
      StringBuilder resultedText = new StringBuilder("");
      resultedText.append(sentence.getSentenceText(), 0, word1.getPositionOfFirstLetter());
      resultedText.append(word2.getWordText());
      resultedText.append(sentence.getSentenceText(), word1.getPositionOfLastLetter(), word2.getPositionOfFirstLetter());
      resultedText.append(word1.getWordText());
      resultedText.append(sentence.getSentenceText(), word2.getPositionOfLastLetter(), sentence.getSentenceText().length());
      return resultedText.toString();
    } else {
      return this.sentence.getSentenceText();
    }
  }



}
