package com.epam.courses.model;

import com.epam.courses.util.Const;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class Book {

  private String bookText;
  private String fileName;

  private List<Sentence> sentences;

  public Book(String fileName) {
    this.fileName = fileName;
    try {
      this.bookText = new String(Files.readAllBytes(Paths.get(fileName)));
    } catch (IOException e) {
      e.printStackTrace();
    }
    sentences = getAllSentences();
  }

  private List<Sentence> getAllSentences() {
    return getWordsByRegexPattern(Const.SENTENCE_PATTERN, bookText).stream()
        .filter(sentence -> !sentence.getSentenceText().isEmpty())
        .collect(Collectors.toList());
  }

  private List<Sentence> getWordsByRegexPattern(Pattern pattern, String stringToSplit) {
    List<Sentence> splittedList = new ArrayList<>();
    Matcher matcher = pattern.matcher(stringToSplit);
    while (matcher.find()) {
      splittedList.add(
          new Sentence(
              stringToSplit.substring(matcher.start(), matcher.end()),
              matcher.start(),
              matcher.end()
          ));
    }
    return splittedList;
  }

  public String getBookText() {
    return bookText;
  }

  public void setBookText(String bookText) {
    this.bookText = bookText;
  }

  public String getFileName() {
    return fileName;
  }

  public void setFileName(String fileName) {
    this.fileName = fileName;
  }

  public List<Sentence> getSentences() {
    return sentences;
  }

  public void setSentences(List<Sentence> sentences) {
    this.sentences = sentences;
  }
}
