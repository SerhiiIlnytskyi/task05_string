package com.epam.courses.model;

public class Word{

  private String wordText;

  private int positionOfFirstLetter;

  private int positionOfLastLetter;

  public Word(String word, int positionOfFirstLetterInSentence, int positionOfLastLetterInSentence) {
    this.wordText = word;
    this.positionOfFirstLetter = positionOfFirstLetterInSentence;
    this.positionOfLastLetter = positionOfLastLetterInSentence;
  }

  public String getWordText() {
    return wordText;
  }

  public void setWordText(String wordText) {
    this.wordText = wordText;
  }

  public int getPositionOfFirstLetter() {
    return positionOfFirstLetter;
  }

  public void setPositionOfFirstLetter(int positionOfFirstLetter) {
    this.positionOfFirstLetter = positionOfFirstLetter;
  }

  public int getPositionOfLastLetter() {
    return positionOfLastLetter;
  }

  public void setPositionOfLastLetter(int positionOfLastLetter) {
    this.positionOfLastLetter = positionOfLastLetter;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    Word word = (Word) o;

    return wordText.equals(word.wordText);

  }

  @Override
  public int hashCode() {
    return wordText.hashCode();
  }

  @Override
  public String toString() {
    return "Word{" +
        "wordText='" + wordText + '\'' +
        ", positionOfFirstLetter=" + positionOfFirstLetter +
        ", positionOfLastLetter=" + positionOfLastLetter +
        '}';
  }
}
