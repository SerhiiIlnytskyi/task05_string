package com.epam.courses.model;

import com.epam.courses.util.Const;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class Sentence {

  private String sentenceText;
  private List<Word> wordList;

  private int positionOfFirstLetter;

  private int positionOfLastLetter;

  public Sentence(String sentenceText, int positionOfFirstLetter, int positionOfLastLetter) {
    this.sentenceText = sentenceText;
    this.positionOfFirstLetter = positionOfFirstLetter;
    this.positionOfLastLetter = positionOfLastLetter;
    this.wordList = getAllWords();
  }

  private List<Word> getAllWords() {
    return getWordsByRegexPattern(Const.WORD_PATTERN).stream()
        .peek(word -> word.setWordText(
            word.getWordText().trim()))
        .peek(word -> word.setWordText(
            Const.SYMBOL_PATTERN.matcher(word.getWordText()).replaceAll("")))
        .filter(word -> !word.getWordText().isEmpty())
        .collect(Collectors.toList());
  }

  public List<Word> getWordsByRegexPattern(Pattern pattern) {
    List<Word> splittedList = new ArrayList<>();
    Matcher matcher = pattern.matcher(sentenceText);
    while (matcher.find()) {
      splittedList.add(
          new Word(
              sentenceText.substring(matcher.start(), matcher.end()),
              matcher.start(),
              matcher.end()
          ));
    }
    return splittedList;
  }


  public String getSentenceText() {
    return sentenceText;
  }

  public void setSentenceText(String sentenceText) {
    this.sentenceText = sentenceText;
  }

  public List<Word> getWordList() {
    return wordList;
  }

  public void setWordList(List<Word> wordList) {
    this.wordList = wordList;
  }

  @Override
  public String toString() {
    return "Sentence{" +
        "sentenceText='" + sentenceText + '\'' +
        ", positionOfFirstLetter=" + positionOfFirstLetter +
        ", positionOfLastLetter=" + positionOfLastLetter +
        '}';
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    Sentence sentence = (Sentence) o;

    return sentenceText.equals(sentence.sentenceText);
  }

  @Override
  public int hashCode() {
    return sentenceText.hashCode();
  }
}
