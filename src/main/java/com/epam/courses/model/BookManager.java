package com.epam.courses.model;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class BookManager {

  private Book book;

  public BookManager(Book book) {
    this.book = book;
  }

  public long numberOfSentencesWithRepitedWords() {
    return book.getSentences().stream()
        .map(SentenceManager::new)
        .filter(sentenceManager -> (sentenceManager.countOfWordsRepetition() > 0))
        .count();
  }

  public List<Sentence> sortSentencesByWordsCount() {
    return book.getSentences().stream()
        .sorted(Comparator.comparing(sentence -> sentence.getWordList().size()))
        .collect(Collectors.toList());
  }

  public Book getBook() {
    return book;
  }

  public void setBook(Book book) {
    this.book = book;
  }
}
