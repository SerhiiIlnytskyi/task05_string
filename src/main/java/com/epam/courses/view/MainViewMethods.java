package com.epam.courses.view;

import com.epam.courses.controller.Controller;
import java.util.LinkedHashMap;
import java.util.Map;

public class MainViewMethods {

  private Controller controller;
  private Map<String, Executable> methodsMenu;

  public MainViewMethods() {
    this.controller = new Controller();
    methodsMenu = new LinkedHashMap<>();
    methodsMenu.put("1", this::button1);
    methodsMenu.put("2", this::button2);
    methodsMenu.put("3", this::button3);
    methodsMenu.put("4", this::button4);
    methodsMenu.put("5", this::button5);
    methodsMenu.put("6", this::button6);
    methodsMenu.put("7", this::button7);
    methodsMenu.put("8", this::button8);
    methodsMenu.put("9", this::button9);
    methodsMenu.put("10", this::button10);
    methodsMenu.put("11", this::button11);
    methodsMenu.put("12", this::button12);
    methodsMenu.put("13", this::button13);
    methodsMenu.put("14", this::button14);
    methodsMenu.put("15", this::button15);
    methodsMenu.put("16", this::button16);
  }

  private void button1() {
    System.out.println(controller.firstTask());
  }

  private void button2() {
    controller.secondTask().stream()
        .forEach(System.out::println);
  }

  private void button3() {
    controller.thirdTask().stream()
        .forEach(System.out::println);
  }

  private void button4() {
    System.out.println("Please enter length of needed words");
    controller.fourthTask(MainView.INPUT.nextInt()).stream()
        .forEach(System.out::println);
  }

  private void button5() {
    controller.fifthTask().stream()
        .forEach(System.out::println);
  }

  private void button6() {
    controller.fodo();
  }

  private void button7() {
    controller.fodo();
  }

  private void button8() {
    controller.fodo();
  }

  private void button9() {
    controller.fodo();
  }

  private void button10() {
    controller.fodo();
  }

  private void button11() {
    controller.fodo();
  }

  private void button12() {
    controller.fodo();
  }

  private void button13() {
    controller.fodo();
  }

  private void button14() {
    controller.fodo();
  }

  private void button15() {
    controller.fodo();
  }

  private void button16() {
    controller.fodo();
  }

  public Map<String, Executable> getMethodsMenu() {
    return methodsMenu;
  }

  public void setMethodsMenu(Map<String, Executable> methodsMenu) {
    this.methodsMenu = methodsMenu;
  }

}
