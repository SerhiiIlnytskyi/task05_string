package com.epam.courses.view;

import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Scanner;

public class MainView {

  static Scanner INPUT = new Scanner(System.in);
  private Map<String, String> menu;
  private MainViewMethods mainViewMethods;

  private ResourceBundle bundle;

  private void setMenu() {
    menu = new LinkedHashMap<>();
    bundle.keySet().stream()
        .sorted((str1, str2) -> new MenuKeysComparator().compare(str1, str2))
        .forEach(key -> menu.put(key, bundle.getString(key)));
  }

  public MainView() {
    Locale locale = new Locale("uk");
    bundle = ResourceBundle.getBundle("MainView", locale);
    setMenu();
    mainViewMethods = new MainViewMethods();
  }

  private void button0() {
    System.out.println("Good luck");
    INPUT.close();
    System.exit(0);
  }

  private void outputMenu() {
    System.out.println("\nMENU:");
    menu.values().forEach(System.out::println);
  }

  public void show() {
    String keyMenu;
    do {
      outputMenu();
      System.out.print("Please, select menu point: ");
      keyMenu = INPUT.nextLine().toUpperCase();
      try {
        mainViewMethods.getMethodsMenu().get(keyMenu).execute();
      } catch (Exception e) {
      }
    } while (!keyMenu.equals("Q"));
    this.button0();
  }
}
