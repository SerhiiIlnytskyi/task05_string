package com.epam.courses.view;

@FunctionalInterface
public interface Executable {
  void execute();
}
