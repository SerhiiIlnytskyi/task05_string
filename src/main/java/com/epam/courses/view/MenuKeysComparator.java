package com.epam.courses.view;

import java.util.Comparator;

public class MenuKeysComparator implements Comparator<String> {
  public int compare(String s1, String s2) {

    String[] s1Parts = s1.split("(?<=\\D)(?=\\d)|(?<=\\d)(?=\\D)");
    String[] s2Parts = s2.split("(?<=\\D)(?=\\d)|(?<=\\d)(?=\\D)");

    int i = 0;
    while(i < s1Parts.length && i < s2Parts.length){
      if(s1Parts[i].compareTo(s2Parts[i]) == 0){
        ++i;
      }else{
        try{
          int intS1 = Integer.parseInt(s1Parts[i]);
          int intS2 = Integer.parseInt(s2Parts[i]);
          int diff = intS1 - intS2;
          if(diff == 0){
            ++i;
          }else{
            return diff;
          }
        }catch(Exception ex){
          return s1.compareTo(s2);
        }
      }
    }
    return Integer.compare(s1.length(), s2.length());
  }
}
